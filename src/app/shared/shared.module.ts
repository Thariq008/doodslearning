import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import {DragDropModule, } from '@angular/cdk/drag-drop';
import {TextFieldModule} from '@angular/cdk/text-field';
import {
  MatSidenavModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
  MatAutocompleteModule,
  MatTabsModule,
  MatSnackBarModule,
  MatExpansionModule,
  MatTooltipModule
} from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTooltipModule,
    DragDropModule,
    TextFieldModule
  ],
  exports :[
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTooltipModule,
    DragDropModule,
    TextFieldModule
  ]
})
export class SharedModule { }
