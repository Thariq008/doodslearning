import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { ToastrService } from 'ngx-toastr';
import { StudentLoginService } from '../student-login/student-login.service';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.scss']
})
export class StudentLoginComponent implements OnInit {

  @ViewChild('pswd', {static: false}) pswdInput:ElementRef;

  loginForm: FormGroup;
  returnUrl: string;
  adminData: any;
  emailId: any;
  initial: string;
  name: string;
  submitted = false;
  btnname: string = "Login";
  loading: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private studentLoginService: StudentLoginService,
    private _snackBar: MatSnackBar,
    // private toastr: ToastrService,
    public router: Router) {
    //  this.adminData = this.router.getCurrentNavigation().extras.state;
    this.loginForm = this.formBuilder.group({
      emailId: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      password: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
  }); 
   }
   get f() { return this.loginForm.controls; }

  ngOnInit() {
    this.pswdInput.nativeElement.focus();
  }

  onSubmit(){
    this.submitted = true;
    this.btnname = "Processing";
    this.loading = true;
    if (!this.loginForm.valid) {
      this.btnname = "Login";
      this.loading = false;
      this._snackBar.open('Please enter your password', 'Close', {
        duration: 6000,
      });
      setTimeout(() => this.pswdInput.nativeElement.focus(), 300);
      return false;
     }

    const loginData = this.loginForm.value;
    const data = {email: loginData.emailId,password: loginData.password}
    this.studentLoginService.login(data).subscribe(
        res => {
      this.loading = false;
          console.log(res.body['status'])
                if (res.body['status'].code === 200) {
                  console.log('in')
                  if (res.body['data'].loginType == 'ADMIN') {
                    sessionStorage.setItem('accessToken', res.body['session'].token);
                    sessionStorage.setItem('name', res.body['data'].name);
                    sessionStorage.setItem('initial', 'T');
  
                    this.router.navigate(['/student/dashboard_student']);
                  } else {
                    sessionStorage.setItem('accessToken', res.body['session'].token);
                    sessionStorage.setItem('name', res.body['data'].name);
                    sessionStorage.setItem('initial', 'T');
                    console.log(res);
                    this.router.navigate(['/student/dashboard_student']);
                  }

                } else {
                  this.loading = false;
                  this.errSnackbar();                  
                }
        },
        err => {
            this.loading = false;
            console.log(err);
        }
    );
   }
   errSnackbar() {
    this.btnname = "Login";
    this.loading = false;
    console.log('55454')
    this._snackBar.open('Password Incorret', 'Close', {
      duration: 7000,
    });
    this.pswdInput.nativeElement.focus();
  }
  //  showSuccess() {
  //   console.log('55454')
  //   this.toastr.warning('Incorrect Password!');
  // }
  }
