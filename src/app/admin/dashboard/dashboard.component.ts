import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  dashboard_history = [
    {
      date: "2019-12-08",
      list:[
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "incomplete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "incomplete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        }
      ]
    },
    {
      date: "2019-12-08",
      list:[
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        }
      ]
    },
    {
      date: "2019-12-08",
      list:[
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        }
      ]
    },
    {
      date: "2019-12-08",
      list:[
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        },
        {
          colg_name: "hindusthan institute of technology", colg_icon: "../../../assets/img/hind_logo.png",
          class: [
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"},
            { name: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete"}
          ],
          trainee: [
            {name: "Rober Williams", img: "../../../assets/img/user.png"},
            {name: "Rober Williams", img: "../../../assets/img/user.png"}
          ]
        }
      ]
    }
  ]


  ngOnInit() {
  }

}
