import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  constructor(private http: HttpClient) { }

  getCustomer() {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/customer', {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  logout() {
    localStorage.clear();
  }

  viewCustomers(id) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/customer/'+id, {

      headers: httpHeaders,
      observe: 'response'
    });
  }
}
