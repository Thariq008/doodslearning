import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../customers/customers.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  customers: any;
  constructor(
    public router: Router,
    private customerService: CustomerService
    ) { }

  ngOnInit() {
    this.customerService.getCustomer().subscribe(
      res => {
    console.log(res)
    this.customers = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }

  // customers = [
  //   {colg_name: "hindusthan institute of technology", location: "coimbatore", colg_icon: "../../../assets/img/hind_logo.png", status:"unassigned"},
  //   {colg_name: "hindusthan institute of technology", location: "coimbatore", colg_icon: "../../../assets/img/hind_logo.png", status:"assigned"},
  // ];

  detailview(id){

    this.router.navigate(['/admin/customers/assign/'+id]);
  }
  assign(){
    this.router.navigate(['/admin/customers/assign']);
  }
}
