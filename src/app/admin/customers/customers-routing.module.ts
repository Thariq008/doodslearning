import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AssignComponent } from './assign/assign.component';

const appRoutes: Routes = [
  { path: '', component: CustomersComponent },
  { path: 'add-customer', component: AddCustomerComponent },
  { path: 'assign', component: AssignComponent },
  { path: 'assign/:id', component: AssignComponent }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
