import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { AssignComponent } from './assign/assign.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [
    CustomersComponent,
    AddCustomerComponent,
    AssignComponent
  ],
  imports: [
    SharedModule,
    CustomersRoutingModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot()
  ]
})
export class CustomersModule { }
