import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { addCustomerService} from './add-customer.service';
import { add } from 'ngx-bootstrap/chronos';
@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  datePickerConfig: Partial<BsDatepickerConfig>;
  minDate: any;
  maxDate: any;
  public imagePath: string | any[];
  imgURL: any;
  myDateValue: Date;
  public message: string;
  loginForm: any;
  M: any;
  D: any;
  Y: any;
  dob: any;
  previousDate: Date;
  addCustomerForm:FormGroup;
  myDateValue1: Date;
  myDateValue2: Date;
  classDate: string;
  startFrom: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public  addCustomerService:addCustomerService
  ) {
    this.addCustomerForm = this.formBuilder.group({
      emailId: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      institutionName: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      city: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      departments: [],
      noOfStudents: ['',[Validators.required, Validators.minLength(1), Validators.maxLength(500)]],
      noOfClass: ['',[Validators.required, Validators.minLength(1), Validators.maxLength(500)]],
      classDuration: ['',[Validators.required, Validators.minLength(20), Validators.maxLength(600)]],
      classDate: [],
      classTime: [],
      startFrom: [],
      courses: ['courses'],
      primaryName: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      primaryDesignation: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      primaryEmail: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      primarymobileNumber: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      secondaryName: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      secondaryDesignation: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      secondaryEmail: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      secondarymobileNumber: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
  });
   }
  get f() { return this.addCustomerForm.controls; }
  ngOnInit() {
    this.minDate = new Date('1950-01-01');
    this.maxDate = new Date('2030-01-01');
    this.myDateValue1 = new Date();
    this.myDateValue2 = new Date();
  }
  preview(files: string | any[]) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  back(){
    this.router.navigate(['/admin/customers']);
  }
  dobEvent(value: Date): void {
    if (value == null) {
      this.dob = '';
    } else {
      var date = value ;
    this.Y = date.getFullYear();
    this.D = date.getDate();
    this.M = date.getMonth() + 1;
    this.dob = this.Y + '-' + this.M + '-' + this.D;
    }
  }
  onSubmit(){
    
    const addCustomerData = this.addCustomerForm.value;
    console.log(addCustomerData)
    const primaryContact={
      name:addCustomerData.primaryName,
      designation:addCustomerData.primaryDesignation,
      email:addCustomerData.primaryEmail,
      mobileNumber:addCustomerData.primarymobileNumber,
    }
    const secondaryContact={
      name:addCustomerData.secondaryName,
      designation:addCustomerData.secondaryDesignation,
      email:addCustomerData.secondaryEmail,
      mobileNumber:addCustomerData.secondarymobileNumber,
    }
    const data = {
      institutionName:addCustomerData.institutionName,city:addCustomerData.city,noOfClass:addCustomerData.noOfClass,
      mobileNumber:"98765432110",classDuration:addCustomerData.classDuration,noOfStudents:addCustomerData.noOfStudents,classDate: this.classDate,
      departments:addCustomerData.departments,startFrom:this.startFrom,primaryContact:primaryContact,secondaryContact:secondaryContact,
      institutionImage:'',courses:["5f269029f696bc5bd60bdafd"]
       
    }
    console.log(data)
    this.addCustomerService.addCustomer(data).subscribe(
      res => {
    console.log(res)
    // this.trainees = res.body['data']
    this.router.navigate(['admin/customers']);
      },
      err => {
          console.log(err);
      }
  );
  }

  onDateChangeClass(newDate: Date) {
    console.log(newDate);
    function convert(str) {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
      return [day, mnth, date.getFullYear()].join("/");
    }
    this.classDate = convert(newDate);
    console.log(this.classDate)
  }

  onDateChangeStart(newDate: Date) {
    console.log(newDate);
    function convert(str) {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
      return [day, mnth, date.getFullYear()].join("/");
    }
    this.startFrom = convert(newDate);
    console.log(this.startFrom)
  }
}