import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
@Injectable({
    providedIn: 'root'
  })
  export class addCustomerService {
  
    baseUrl = environment.baseUrl;
    accessToken = sessionStorage.getItem('accessToken');
    constructor(private http: HttpClient) { }
  
    addCustomer(data) {
  
      const httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.accessToken
      });
      return this.http.post(this.baseUrl + '/customer', data, {
  
        headers: httpHeaders,
        observe: 'response'
      });
    }
  
    logout() {
      localStorage.clear();
    }
  
  }