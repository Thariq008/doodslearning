import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {TraineeListService} from '../trainee-list/trainee-list.service'
import Swal from 'sweetalert2';

@Component({
  selector: 'app-trainee-list',
  templateUrl: './trainee-list.component.html',
  styleUrls: ['./trainee-list.component.scss']
})
export class TraineeListComponent implements OnInit {
  trainees: any;
  constructor(
    public router: Router,
    private traineeListService: TraineeListService
    ) { }

  ngOnInit() {
this.traineeList();
  }
  traineeList() {
    this.traineeListService.getCustomer().subscribe(
      res => {
    console.log(res)
    this.trainees = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }
  detailview(id){
  //   this.traineeListService.getCustomer().subscribe(
  //     res => {
  //   console.log(res)
  //   this.trainees = res.body['data']
  //     },
  //     err => {
  //         console.log(err);
  //     }
  // );
    this.router.navigate(['/admin/trainee-list/view-trainee/'+id]);
  }
  deleteTrainee(id){
    Swal({
      title: 'Are you sure?',
      text: 'You wont be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        // const data = {cuisinesId: id};

        this.traineeListService.deleteTrainee(id).subscribe(
          res => {
            if (res.body['status'].code == 200) {
              Swal(
                'Deleted!',
                'Your file has been deleted.',
                'success'
              );
this.traineeList();            
            } else {
              Swal(
                'Not Deleted',
                'Something Went Wrong!',
                'warning'
              );
            }

          },
          err => {
            this.router.navigateByUrl('/serverError');
          }
        );
      }
    });
  }
}
