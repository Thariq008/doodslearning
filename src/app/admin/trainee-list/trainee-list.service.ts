import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TraineeListService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  constructor(private http: HttpClient) { }

  deleteTrainee(data) {
console.log(data)
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.delete(this.baseUrl + '/trainee/' + data, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  logout() {
    localStorage.clear();
  }

  getCustomer() {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/trainee', {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  getTrainee(id) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/trainee/' + id, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

}
