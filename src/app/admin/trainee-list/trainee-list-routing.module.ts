import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeListComponent } from './trainee-list.component';
import { CreateTraineeComponent } from './create-trainee/create-trainee.component';
import { ViewTraineeComponent } from './view-trainee/view-trainee.component';

const appRoutes: Routes = [
  { path: '', component: TraineeListComponent },
  { path: 'create-trainee', component: CreateTraineeComponent },
  { path: 'view-trainee', component: ViewTraineeComponent },
  { path: 'view-trainee/:id', component: ViewTraineeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class TraineeListRoutingModule { }
