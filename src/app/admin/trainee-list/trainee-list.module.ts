import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TraineeListRoutingModule } from './trainee-list-routing.module';
import { TraineeListComponent } from './trainee-list.component';
import { CreateTraineeComponent } from './create-trainee/create-trainee.component';
import { ViewTraineeComponent } from './view-trainee/view-trainee.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [
    TraineeListComponent,
    CreateTraineeComponent,
    ViewTraineeComponent
  ],
  imports: [
    SharedModule,
    TraineeListRoutingModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot()
  ]
})
export class TraineeListModule { }
