import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { createTraineeService} from './create-trainee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { add } from 'ngx-bootstrap/chronos';
@Component({
  selector: 'app-create-trainee',
  templateUrl: './create-trainee.component.html',
  styleUrls: ['./create-trainee.component.scss']
})
export class CreateTraineeComponent implements OnInit {
  public imagePath: string | any[];
  imgURL: any;
  public message: string;
  addTraineeForm:FormGroup;
  dateOfBirth: any;
  constructor(public router: Router,
    private formBuilder: FormBuilder,
    public CreateTraineeService:createTraineeService
    ) { 
      this.addTraineeForm = this.formBuilder.group({
        name: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        email: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        mobileNumber: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        dateOfBirth: [''],
        password: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        gender: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        location: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        education: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        lastJob: ['',],
        yearsOfExperiance: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
        skills: ['',],
        image: ['',],
    }); 
    }
   get f() { return this.addTraineeForm.controls; }
  ngOnInit() {
  }
 
  preview(files: string | any[]) {
    debugger;
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  back(){
    this.router.navigate(['/admin/trainee-list']);
  }
  submit(){
    
    const addTraineeData = this.addTraineeForm.value;
    const data = {name: addTraineeData.name,email: addTraineeData.email,mobileNumber:addTraineeData.mobileNumber,
dateOfBirth:this.dateOfBirth,password:addTraineeData.password,gender:addTraineeData.gender,
location:addTraineeData.location,education:addTraineeData.education,lastJob:addTraineeData.lastJob,yearsOfExperiance:addTraineeData.yearsOfExperiance,skills:addTraineeData.skills,image:addTraineeData.image
    }
    this.CreateTraineeService.addtrainee(data).subscribe(
      res => {
    // this.trainees = res.body['data']
    this.router.navigate(['admin/trainee-list']);
      },
      err => {
          console.log(err);
      }
  );
  }

  onDateChange(newDate: Date) {
    console.log(newDate);
    function convert(str) {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
      return [day, mnth, date.getFullYear()].join("/");
    }
    this.dateOfBirth = convert(newDate);
    console.log(this.dateOfBirth)
  } 
}