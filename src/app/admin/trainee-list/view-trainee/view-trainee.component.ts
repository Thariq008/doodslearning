import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {TraineeListService} from '../../trainee-list/trainee-list.service'

@Component({
  selector: 'app-view-trainee',
  templateUrl: './view-trainee.component.html',
  styleUrls: ['./view-trainee.component.scss']
})
export class ViewTraineeComponent implements OnInit {

  prf_details: any;
  prf_history: any;
  traineeId: any;
  name: any;
  email: any;
  mobileNumber: any;
  dateOfBirth: any;
  password: any;
  location: any;
  gender: any;
  education: any;
  lastJob: any;
  yearsOfExperience: any;
  paymentname: any;
  dishes: any;
  userName: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private traineeListService: TraineeListService
    ) { }

  profile: any= 
  {
    fullname: "Robert William's", uid: "US3R1D001", src: "../../../assets/img/user.png", location: "Chennai", 
    details:{
      email_id: "roberwilliams@gmail.com", mobile_no: "+91_9876543210", dob: "2019-12-08", sex: "male", edu: "B.E Computer Science Engineering", last_work: "Cognizant, Frontend Developer", work_exp: "2years", 
      skill:["graphic designer", "wordpress", "html5", "css3", "marketing", "angular"]
    },
    history:{
      cls_total: "20", cmpl_cls: "19", incmpl: "1",
      cls_list:[
        {
          date: "2019-12-08",
          list:[
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "incomplete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"}
          ]
        },
        {
          date: "2019-12-08",
          list:[
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"}
          ]
        },
        {
          date: "2019-12-08",
          list:[
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"}
          ]
        },
        {
          date: "2019-12-08",
          list:[
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"}
          ]
        }
      ]
    }
  };

  ngOnInit() {
    this.route.params.subscribe( params => this.traineeId = params.id );
    this.traineeListService.getTrainee(this.traineeId).subscribe (
      response => {
console.log(response)
         const traineeDetails = response.body['data'];
         this.name = traineeDetails.name;
         this.email = traineeDetails.email;
         this.mobileNumber = traineeDetails.mobileNumber;
         this.dateOfBirth = traineeDetails.dateOfBirth;
         this.password = traineeDetails.password;
         this.gender = traineeDetails.gender;
         this.location = traineeDetails.location;
         this.education = traineeDetails.education;
         this.lastJob = traineeDetails.lastJob;
         this.yearsOfExperience = traineeDetails.yearsOfExperience;
      },
      err => {
        // this.router.navigateByUrl('/serverError');
      }
    );
    this.prf_details = this.profile.details;
    this.prf_history = this.profile.history;
  }


  back(){
    this.router.navigate(['/admin/trainee-list']);
  }

  

}
