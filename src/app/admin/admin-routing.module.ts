import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const appRoutes: Routes = [
  {
    path: '', component: AdminComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'customers', loadChildren: () => import(`./customers/customers.module`).then(m => m.CustomersModule) },
      { path: 'trainee-list', loadChildren: () => import(`./trainee-list/trainee-list.module`).then(m => m.TraineeListModule) },
      { path: 'courses', loadChildren: () => import(`./courses/courses.module`).then(m => m.CoursesModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
