import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from './courses.component' ;
import { AddCourseComponent } from './add-course/add-course.component';
import { ViewCourseComponent } from './view-course/view-course.component';
import { AddTopicsComponent } from './add-topics/add-topics.component';
import { from } from 'rxjs';

const appRoutes: Routes = [
  { path: '', component: CoursesComponent },
  { path: 'add-course', component: AddCourseComponent },
  { path: 'add-course/:id', component: AddCourseComponent },
  { path: 'view-course', component: ViewCourseComponent },
  { path: 'view-course/:id', component: ViewCourseComponent },
  { path: 'add-topics', component: AddTopicsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
