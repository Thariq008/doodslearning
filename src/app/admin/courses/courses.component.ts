import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddTopicsService } from './courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses: any;

  constructor(
    public router: Router,
    public addTopicsService: AddTopicsService
    ) { }

  ngOnInit() {
    this.CoursesList();
  }
  CoursesList() {
    this.addTopicsService.addTopics().subscribe(
      res => {
    this.courses = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }

  detailview(id){
    console.log(id)
    this.router.navigate(['/admin/courses/add-course/'+id]);
  }
  coursesView(id){
    this.router.navigate(['/admin/courses/view-course/'+id]);
  }
}
