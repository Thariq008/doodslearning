import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {AddTopicsService} from './add-topics.service';

@Component({
  selector: 'app-add-topics',
  templateUrl: './add-topics.component.html',
  styleUrls: ['./add-topics.component.scss']
})
export class AddTopicsComponent implements OnInit {

  public imagePath: string | any[];
  imgURL: any;
  public message: string;

  @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;
  addTopicsForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    public addTopicsService : AddTopicsService
    ) { 
      this.addTopicsForm = this.formBuilder.group({
        name: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      })
    }
  get f() { return this.addTopicsForm.controls; }


  ngOnInit() {

  }

  onSubmit(){
    const addTopicsData = this.addTopicsForm.value;

    const data = {
      name:addTopicsData.name
    }
    this.addTopicsService.addTopics(data).subscribe(
      res => {
    // this.trainees = res.body['data']
    this.router.navigate(['admin/courses']);
      },
      err => {
          console.log(err);
      }
  );
  }

  back(){
    this.router.navigate(['/admin/courses']);
  }
}

