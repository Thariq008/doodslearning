import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { AddCourseService } from './add-course.service';
declare var CKEDITOR: any;

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {

  public imagePath: string | any[];
  imgURL: any;
  public message: string;

  @ViewChild('autosize', {static: false}) autosize: CdkTextareaAutosize;
  CoursesId: any;
  movies: any;
  page = 1;
  addCourseForm: FormGroup;
  public ckeditorContent: string = "";

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    public addCourseService: AddCourseService,
    public route: ActivatedRoute
    ) {
      this.addCourseForm = this.formBuilder.group({
        name: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
        description: ['',],
        question: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
        answer: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
        output: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]]
      })
     }
  get f() { return this.addCourseForm.controls; }

  ngOnInit() {
    this.CoursesList();
    CKEDITOR.on("instanceCreated", function(event, data) {
      var editor = event.editor,
        element = editor.element;
      editor.name = "content";
    });
  }
  CoursesList() {
    this.route.params.subscribe( params => this.CoursesId = params.id );
    this.addCourseService.viewCourses(this.CoursesId).subscribe(
      res => {
        console.log(res)
    this.movies = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }
  public model = {
    editorData: 'Write your topic description'
  };
  // movies = [
  //   'Episode I - The Phantom Menace',
  //   'Episode II - Attack of the Clones',
  //   'Episode III - Revenge of the Sith',
  //   'Episode IV - A New Hope',
  //   'Episode V - The Empire Strikes Back',
  //   'Episode VI - Return of the Jedi',
  //   'Episode VII - The Force Awakens',
  //   'Episode VIII - The Last Jedi',
  //   'Episode IX – The Rise of Skywalker'
  // ];

  preview(files: string | any[]) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.movies, event.previousIndex, event.currentIndex);
  }
  back(){
    this.router.navigate(['/admin/courses']);
  }

  onSubmit(){
    console.log(CKEDITOR.instances.content);

    const addCourseForm = this.addCourseForm.value;
    console.log(this.addCourseForm.value,'*****')
    const data = {
      name:addCourseForm.name,content:addCourseForm.description,question:addCourseForm.question,answer:addCourseForm.answer,
      output: addCourseForm.output
       
    }
    console.log(data)
    this.addCourseService.addCourse(data,this.CoursesId).subscribe(
      res => {
    console.log(res)
    // this.trainees = res.body['data']
    // this.router.navigate(['admin/courses/add-course/'+this.CoursesId]);
    this.router.navigate(['admin/courses']);
      },
      err => {
          console.log(err);
      }
  );
  } 
}
