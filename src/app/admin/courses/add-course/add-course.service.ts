import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class AddCourseService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  constructor(private http: HttpClient) { }
  
  viewCourses(id){
    const httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.accessToken
      });
      return this.http.get(this.baseUrl + '/course/'+id+'/topics', {
  
        headers: httpHeaders,
        observe: 'response'
      });
  }
  addCourse(data,id){
    const httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.accessToken
      });
      return this.http.post(this.baseUrl + '/course/'+id+'/topic',data, {
  
        headers: httpHeaders,
        observe: 'response'
      });
  }
}
