import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { CoursesRoutingModule } from './courses-routing.module';
import { CoursesComponent } from './courses.component' ;
import { AddCourseComponent } from './add-course/add-course.component';
import { ViewCourseComponent } from './view-course/view-course.component';
import { AddTopicsComponent } from './add-topics/add-topics.component';



@NgModule({
  declarations: [
    CoursesComponent,
    AddCourseComponent,
    ViewCourseComponent,
    AddTopicsComponent,
  ],
  imports: [
    SharedModule,
    CKEditorModule,
    CoursesRoutingModule
  ]
})
export class CoursesModule { }
