import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule,  } from '@angular/common/http'; 
import { ToastrModule } from 'ngx-toastr';
// import { HttpModule } from '@angular/http';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Page404ErrorComponent } from './page404-error/page404-error.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule } from '@angular/forms';
import { TraineeLoginComponent } from './trainee-login/trainee-login.component';
import { StudentLoginComponent } from './student-login/student-login.component';
// import { TraineeComponent } from './trainee/trainee.component';
import { CoursesListComponent } from './trainee/courses-list/courses-list.component';
import { CoursesViewComponent } from './trainee/courses-view/courses-view.component';
import { StudentListComponent } from './trainee/student-list/student-list.component';
import { AddStudentComponent } from './trainee/add-student/add-student.component';
import { ViewStudentComponent } from './trainee/view-student/view-student.component';

@NgModule({
  declarations: [
    AppComponent,
    Page404ErrorComponent,
    TraineeLoginComponent,
    StudentLoginComponent,
    // TraineeComponent,
    CoursesListComponent,
    CoursesViewComponent,
    StudentListComponent,
    AddStudentComponent,
    ViewStudentComponent,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    FormsModule 
    // HttpModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
