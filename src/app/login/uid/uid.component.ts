import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UidService } from './uid.service';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { ToastrService } from 'ngx-toastr';
// import { LoginModule } from '../login.module';


@Component({
  selector: 'app-uid',
  templateUrl: './uid.component.html',
  styleUrls: ['../login.component.scss']
})
export class UidComponent implements OnInit {

  @ViewChild('uid', {static: false}) uidInput:ElementRef;

  loginForm: FormGroup;
  returnUrl: string;
  btnname: string = "Next";
  submitted = false;
  loading: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private uidService: UidService,
    private _snackBar: MatSnackBar

  ) {
    this.loginForm = this.formBuilder.group({
      emailId: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
  });
   }
   get f() { return this.loginForm.controls; }

  ngOnInit() {
    this.uidInput.nativeElement.focus();
    console.log(this.formBuilder);
    if (localStorage.getItem('accessToken')) {
        this.router.navigateByUrl(this.returnUrl);
    }
      const body = document.getElementsByTagName('body')[0];
      body.classList.add('login-page');
  }
  // next(){
  //   this.router.navigate(['/login/pswd'])
  // }
  onSubmit() {
    this.submitted = true;
    this.btnname = "Processing";
    this.loading = true;
    if (!this.loginForm.valid) {
      this.btnname = "Next";
      this.loading = false;
      this._snackBar.open('Please enter your valid email', 'Close', {
        duration: 6000,
      });
      setTimeout(() => this.uidInput.nativeElement.focus(), 300);
      return false;
     }

    const loginData = this.loginForm.value;
    var data = {emailId: loginData.emailId}
                  this.router.navigate(['/login/pswd'],{state: data});
    // this.loginSaveData = new LoginModel(loginData.emailId, loginData.password);
    // this.uidService.post(loginData).subscribe(
    //     res => {
    //             if (res.body['error'] === true) {
    //             this.errSnackbar();
    //             } else {
    //               console.log(res.body)
    //               var data = {emailId: res.body['emailId'], name: res.body['name']}
    //               this.router.navigate(['/login/pswd'],{state: data});
    //               // this.router.navigate(['/login/pswd'])
    //             }
    //     },
    //     err => {
    //         console.log(err);
    //     }
    // );
}
errSnackbar() {
  this.btnname = "Next";
  this.loading = false;
  console.log('55454')
  this._snackBar.open('UID Incorret', 'Close', {
    duration: 7000,
  });
  this.uidInput.nativeElement.focus();
  // this.toastr.warning('UID Incorrect!');
}
}

