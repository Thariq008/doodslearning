import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Animations } from './../animation';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    Animations.loginAnimation
  ]
})
export class LoginComponent implements OnInit {

  constructor(activerouter: ActivatedRoute) { }

  ngOnInit() {
  }

  getstate(outlet: any){
    return outlet.activatedRouteData['status'];
  }

}
