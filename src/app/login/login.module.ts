import { NgModule } from '@angular/core';
import { RouterModule, Routes, ActivationStart } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login.component'
import { UidComponent } from './uid/uid.component';
import { PasswordComponent } from './password/password.component';

const appRoutes: Routes = [
  {
    path: '', component: LoginComponent,

    children: [

      // { path: '', component: UidComponent, pathMatch: 'full', data: { status: 1 } },
      // { path: 'uid', component: UidComponent, data: { status: 1 } },
      // { path: 'pswd', component: PasswordComponent, data: { status: 2 } },

      { path: '', component: UidComponent, data: { status: 1 }},
      { path: 'uid', component: UidComponent, data: { status: 1 } },
      { path: 'pswd', component: PasswordComponent, data: { status: 2 } }

    ]

  }
];

@NgModule({
  declarations: [
    LoginComponent,
    UidComponent,
    PasswordComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(appRoutes)
  ],
  exports: [RouterModule]
})
export class LoginModule { }
