import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class TraineeLoginService {

  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  login(apiData) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    console.log(apiData)
    return this.http.post(this.baseUrl + '/login?role=trainee', apiData, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  logout() {
    localStorage.clear();
  }

}
