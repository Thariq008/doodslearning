import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class DailyClassService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  constructor(private http: HttpClient) { }

  getCourse(id) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/course/' + id+ '/topics', {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  getCourseDetails(coursesId,topicsId) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/course/' + coursesId+ '/topic/'+ topicsId, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  addTopics(){
    const httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.accessToken
      });
      return this.http.get(this.baseUrl + '/course', {
  
        headers: httpHeaders,
        observe: 'response'
      });
  }  

}
