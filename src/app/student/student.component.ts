import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  adminData: any;
  name: string;
  initial: string;
  constructor(public router: Router) {
    this.adminData = this.router.getCurrentNavigation().extras.state;

   }

  ngOnInit() {

    this.name = sessionStorage.getItem('name');
    this.initial = sessionStorage.getItem('initial');

    // this.name = this.adminData.name
    // this.initial = this.adminData.name.charAt(0)
  }
logout(){
  sessionStorage.clear();
  this.router.navigate(['/']);
}
}
