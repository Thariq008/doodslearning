import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { StudentRoutingModule } from './student-routing.module';
import { StudentComponent } from './student.component';
import { DashboardStudComponent } from './dashboard-stud/dashboard-stud.component';
import { DailyClassComponent } from './daily-class/daily-class.component';
import { CompletedCourseComponent } from './completed-course/completed-course.component';
import { StudentCompilerComponent } from './student-compiler/student-compiler.component';


@NgModule({
  declarations: [
    StudentComponent,
    DashboardStudComponent,
    DailyClassComponent,
    CompletedCourseComponent,
    StudentCompilerComponent  ],
  imports: [
    SharedModule,
    StudentRoutingModule
  ]
})
export class StudentModule { }
