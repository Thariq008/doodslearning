import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentComponent } from './student.component';
import { DashboardStudComponent } from './dashboard-stud/dashboard-stud.component';
import { DailyClassComponent } from './daily-class/daily-class.component';
import { CompletedCourseComponent } from './completed-course/completed-course.component';
import { StudentCompilerComponent } from './student-compiler/student-compiler.component';

const appRoutes: Routes = [
  {
    path: '', component: StudentComponent,
    children: [
      { path: '', component: DashboardStudComponent },
      { path: 'dashboard_student', component: DashboardStudComponent },
      { path: 'daily_class', component: DailyClassComponent },
      { path: 'daily_class/:id', component: DailyClassComponent },
      { path: 'completed_courses', component: CompletedCourseComponent },
      { path: 'student_compiler', component: StudentCompilerComponent },
      // { path: 'completed_courses', loadChildren: () => import(`./completed-course/completed-course.module`).then(m => m.CompletedCourseModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
