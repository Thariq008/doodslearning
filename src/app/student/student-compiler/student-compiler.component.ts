import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StudentCompilerService } from './student-compiler.service';
import * as $ from './ajax.jquery.js';

@Component({
  selector: 'app-student-compiler',
  templateUrl: './student-compiler.component.html',
  styleUrls: ['./student-compiler.component.scss']
})
export class StudentCompilerComponent implements OnInit {
  studentCompilerForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private StudentCompilerService: StudentCompilerService) {
      this.studentCompilerForm = this.formBuilder.group({
      code: ['',[Validators.required]],
  }); 
   }

  ngOnInit() {

  }

  execute(){
    if(this.studentCompilerForm.value.code){
      var to_compile = {
        "LanguageChoice": "6",
        "Program": this.studentCompilerForm.value.code,
        "Input": "",
        "CompilerArgs" : "-o a.out source_file.c"
      };
        $.ajax ({
            url: "https://rextester.com/rundotnet/api",
            type: "POST",
            data: to_compile
        }).done(function(data) {
          $('#picoutput').html(JSON.stringify(data));
            this.responseData = JSON.stringify(data);
            console.log('resp',this.responseData)
        }).fail(function(data, err) {
            alert("fail " + JSON.stringify(data) + " " + JSON.stringify(err));
        });
    } else {
      console.log("code not available")
    }
  }

}
