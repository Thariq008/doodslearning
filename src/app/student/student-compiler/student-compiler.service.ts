import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
import * as $ from './ajax.jquery.js';

@Injectable({
  providedIn: 'root'
})
export class StudentCompilerService {

  baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) { }

  compile(requestData) {
      $.ajax ({
          url: "https://rextester.com/rundotnet/api",
          type: "POST",
          data: requestData
      }).done(function(data) {
        return data;
          // alert(JSON.stringify(data));
      }).fail(function(data, err) {
          alert("fail " + JSON.stringify(data) + " " + JSON.stringify(err));
      });
  }
}
