import { TestBed } from '@angular/core/testing';

import { StudentCompilerService } from './student-compiler.service';

describe('StudentCompilerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentCompilerService = TestBed.get(StudentCompilerService);
    expect(service).toBeTruthy();
  });
});
