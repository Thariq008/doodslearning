import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCompilerComponent } from './student-compiler.component';

describe('StudentCompilerComponent', () => {
  let component: StudentCompilerComponent;
  let fixture: ComponentFixture<StudentCompilerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCompilerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCompilerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
