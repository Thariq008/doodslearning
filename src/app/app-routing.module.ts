import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Page404ErrorComponent } from './page404-error/page404-error.component';
import { TraineeLoginComponent } from './trainee-login/trainee-login.component';
import { StudentLoginComponent } from './student-login/student-login.component';
const appRoutes: Routes = [
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '', component: StudentLoginComponent},
  { path: 'studentLogin', component: StudentLoginComponent},
  { path: 'trainee', component: TraineeLoginComponent},
  { path: 'login', loadChildren: () => import(`./login/login.module`).then(m => m.LoginModule) },
  { path: 'admin', loadChildren: () => import(`./admin/admin.module`).then(m => m.AdminModule) },
  { path: 'student', loadChildren: () => import(`./student/student.module`).then(m => m.StudentModule) },
  { path: 'trainee', loadChildren: () => import(`./trainee/trainee.module`).then(m => m.TraineeModule) },
  { path: 'compiler', loadChildren: () => import(`./student/student.module`).then(m => m.StudentModule) },
  { path: "**", component: Page404ErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
