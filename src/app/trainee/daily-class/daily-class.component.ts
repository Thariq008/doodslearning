import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DailyClassService } from '../daily-class/daily-class.service'
@Component({
  selector: 'app-daily-class',
  templateUrl: './daily-class.component.html',
  styleUrls: ['./daily-class.component.scss']
})
export class DailyClassComponent implements OnInit {
  selectedTopic: any;
  topicName: any;
  coursesId: any;
  
  topics: any = [
    {
      list: [
        {name: "Hashings", id: "0"}, 
        {name: "Inheritance and polymorphism", id: "1"}, 
        {name: "Recursion",id: "2"}, {name: "Classes", id: "3"},
        {name: "System level operations", id: "4"},
        {name: "File handling", id: "5"},
        {name: "Conditional statements and switches",id: "6"},
        {name: "Arrays", id: "7"},
        {name: "Pointers", id: "8"},
        {name: "Functions", id: "9"},
        {name: "Structures",id: "10"},
        {name: "Variables and handling datatypes", id: "11"}
      ]}
  ]
  name: any;
  question: any;
  answer: any;
  output: any;
  content: any;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private dailyClassService: DailyClassService
    ) { }

  ngOnInit() {
    this.route.params.subscribe( params => this.coursesId = params.id );
    // this.topicName = this.topics[0].list[0].name;
    // this.selectedTopic = this.topics[0].list[0].id;

        if (this.coursesId == undefined){
        this.dailyClassService.addTopics().subscribe(
          res => {
            console.log(res)
        this.coursesId = res.body['data'][0]._id
        this.viewCourses();
          },
          err => {
              console.log(err);
          }
        );
        } else {
        this.viewCourses();
        }
                
        }

  viewCourses(){
    this.dailyClassService.getCourse(this.coursesId).subscribe (
      response => {
      this.topics = response.body['data']
      this.dailyClassService.getCourseDetails(this.coursesId,this.topics[0]._id).subscribe (
        res => {
       this.selectedTopic = res.body['data'].name
       this.topicName = res.body['data'].name
       this.content = res.body['data'].content
       this.question = res.body['data'].question
       this.answer = res.body['data'].answer
       this.output = res.body['data'].output
        })
      })
  }

  contentView(topicList: any) {
    this.selectedTopic = topicList._id;
    // this.topicName = topicList.name;
    this.dailyClassService.getCourseDetails(this.coursesId,topicList._id).subscribe (
      res => {
     this.topicName = res.body['data'].name
     this.content = res.body['data'].content
     this.question = res.body['data'].question
     this.answer = res.body['data'].answer
     this.output = res.body['data'].output
      })

  }

  runCompiler(){
    console.log(this.selectedTopic);
  }

  back(){
    this.router.navigate(['/trainee/completed_courses']);
  }
}
