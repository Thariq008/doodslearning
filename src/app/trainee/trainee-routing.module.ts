import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TraineeComponent } from './trainee.component';
// import { CoursesListComponent } from '../trainee/courses-list/courses-list.component'
// import { CoursesViewComponent } from '../trainee/courses-view/courses-view.component';
// import { StudentListComponent } from '../trainee/student-list/student-list.component';
// import { AddStudentComponent } from '../trainee/add-student/add-student.component';
// import { ViewStudentComponent } from '../trainee/view-student/view-student.component';
import { DashboardTraineeComponent } from './dashboard-trainee/dashboard-trainee.component';
import { CompletedCourseComponent } from './completed-course/completed-course.component';
import { DailyClassComponent } from './daily-class/daily-class.component';

const appRoutes: Routes = [
  {
    path: '', component: TraineeComponent,
    children: [
      { path: '', component: DashboardTraineeComponent },
      { path: 'dashboard-trainee', component: DashboardTraineeComponent },
      { path: 'students', loadChildren: () => import(`./students/students.module`).then(m => m.StudentsModule) },
      { path: 'student', loadChildren: () => import(`./trainee.module`).then(m => m.TraineeModule) },
      { path: 'student-list', loadChildren: () => import(`./trainee.module`).then(m => m.TraineeModule) },
      { path: 'courses', loadChildren: () => import(`./trainee.module`).then(m => m.TraineeModule) },
      { path: 'daily_class/:id', component: DailyClassComponent  },
      { path: 'completed_courses', component: CompletedCourseComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class TraineeRoutingModule { }
