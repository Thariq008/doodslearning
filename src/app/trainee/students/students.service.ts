import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  traineeId = sessionStorage.getItem('traineeId');
  constructor(private http: HttpClient) { }

  getStudent(customerId) {
    console.log(customerId);
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/student/customer/' +customerId, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  logout() {
    localStorage.clear();
  }

  viewStudents(id) {

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.accessToken
    });
    return this.http.get(this.baseUrl + '/student/'+id, {

      headers: httpHeaders,
      observe: 'response'
    });
  }

  getCustomer() {
    
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZDZkM2ZkYzZmZDMzMzFlMTgwNjhhMSIsInVzZXJUeXBlIjoiQURNSU4iLCJpYXQiOjE2MDA2ODQzMDAsImV4cCI6MTYwMDc3MDcwMH0.Es3w4uhd6ZOyTa777D9YNOF5CCxrXEdqc0kjymuSX8U'
    });
    return this.http.get(this.baseUrl + '/customer', {

      headers: httpHeaders,
      observe: 'response'
    });
  }
}
