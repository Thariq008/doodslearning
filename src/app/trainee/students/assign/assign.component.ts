import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StudentService } from '../../students/students.service';


@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit {

  prf_details: any;
  prf_history: any;
  assign_status: boolean = true;
  studentsId: any;
  classDate: any;
  name: any;
  email: any;
  collegeName: any;
  mobileNumber: any;
  dateOfBirth: any;
  gender: any;
  degree: any;
  location: any;
  city: any;
  noOfClass: any;
  classDuration: any;
  noOfStudents: any;
  startFrom: any;
  classTime: any;
  lastJob: any;
  yearsOfExperience: any;
  primaryName: any;
  primaryDesignation: any;
  primaryEmail: any;
  primaryMobileNumber: any;
  secondaryName: any;
  secondaryDesignation: any;
  secondaryEmail: any;
  secondaryMobileNumber: any;
  departments: any;
  officials: { name: string; designation: string; mailid: string; mobile: string; photo: string; }[];

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private studentService: StudentService
  ) { }

  profile: any= 
  {
    col_name: "hindusthan institute of technology", src: "../../../assets/img/hind_logo.png", location: "Coimbatiore", status:"unassigned",
    details:{
      start: "2019-12-08", days: "Every Firday", time: "2pm-5pm", total_std: "30", dept: ["b.e-cse", "b.e-ece"], n_class: "14", day_hr:"3", 
      courses: [
        {name:"basic c", icon:"../../../assets/img/c-programming.png"},
        {name:"basic c", icon:"../../../assets/img/c-programming.png"}
      ]
      // , 
      // officials:[
      //   {name:"Seetha Raman", designation:"principal", mailid:"seetharamank34@gmail.com", mobile:"+919876543210", photo:"../../../assets/img/user.png"},
      //   {name:"Pavithran", designation:"H.O.D", mailid:"pavithran26@gmail.com", mobile:"+919785642310", photo:"../../../assets/img/user.png"}
      // ]
    },
    history:{
      cls_total: "20", cmpl_cls: "19", incmpl: "1",
      cls_list:[
        {
          date: "2019-12-08",
          list:[
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "incomplete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"},
            { language: "Basic C", topic: "Inheritance", lang_icon: "../../../assets/img/c-programming.png", status: "complete", colg: "hindusthan institute of technology"}
          ]
        }],
        assign_list:[
          {
            date: "2019-12-08",
            trainees:[
              { name: "Robert William's", photo: "../../../assets/img/user.png"},
              { name: "Robert William's", photo: "../../../assets/img/user.png"}
            ]
          },
          {
            date: "2019-12-08",
            trainees:[
              { name: "Robert William's", photo: "../../../assets/img/user.png"},
              { name: "Robert William's", photo: "../../../assets/img/user.png"}
            ]
          },
          {
            date: "2019-12-08",
            trainees:[
              { name: "Robert William's", photo: "../../../assets/img/user.png"},
              { name: "Robert William's", photo: "../../../assets/img/user.png"}
            ]
          },
          {
            date: "2019-12-08",
            trainees:[
              { name: "Robert William's", photo: "../../../assets/img/user.png"},
              { name: "Robert William's", photo: "../../../assets/img/user.png"}
            ]
          },
          {
            date: "2019-12-08",
            trainees:[
              { name: "Robert William's", photo: "../../../assets/img/user.png"},
              { name: "Robert William's", photo: "../../../assets/img/user.png"}
            ]
          }
        ]
      }
  };

  ngOnInit() {
    this.route.params.subscribe( params => this.studentsId = params.id );
    this.studentService.viewStudents(this.studentsId).subscribe (
      response => {
         const studentDetails = response.body['data'];
        //  const primaryContact = response.body['data'].primaryContact
        //  const secondaryContact = response.body['data'].secondaryContact
         // primary
        //  this.primaryName = primaryContact.name
        //  this.primaryDesignation = primaryContact.designation
        //  this.primaryEmail = primaryContact.email
        //  this.primaryMobileNumber = primaryContact.mobileNumber


         //secondary
        //  this.secondaryName = primaryContact.name
        //  this.secondaryDesignation = primaryContact.designation
        //  this.secondaryEmail = primaryContact.email
        //  this.secondaryMobileNumber = primaryContact.mobileNumber

        //  this.officials =[
        //   {name:this.primaryName, designation:this.primaryDesignation, mailid:this.primaryEmail, mobile:this.primaryMobileNumber, photo:"../../../assets/img/user.png"},
        //   {name:this.secondaryName, designation:this.secondaryDesignation, mailid:this.secondaryEmail, mobile:this.secondaryMobileNumber, photo:"../../../assets/img/user.png"}
        // ]
         this.name = studentDetails.name
         this.email = studentDetails.email;
         this.mobileNumber = studentDetails.mobileNumber;
         this.dateOfBirth = studentDetails.dateOfBirth;
         this.gender = studentDetails.gender;
         this.collegeName = studentDetails.collegeName;
         this.degree = studentDetails.degree;
         this.location = studentDetails.location;
      },
      err => {
        // this.router.navigateByUrl('/serverError');
      }
    );
    this.prf_details = this.profile.details;
    this.prf_history = this.profile.history;
  }

  back(){
    this.router.navigate(['/trainee/students']);
  }

  assign(){
    this.assign_status = !this.assign_status;
  }
}
