import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { addStudentService} from './add-student.service';
import { add } from 'ngx-bootstrap/chronos';
@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {
  traineeId = sessionStorage.getItem('traineeId');
  public imagePath: string | any[];
  imgURL: any;
  public message: string;
  addStudentForm:FormGroup;
  dateOfBirth: any;
  customer: any;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public  addStudentService:addStudentService
  ) {
    this.addStudentForm = this.formBuilder.group({
      email: ['',[Validators.required, Validators.minLength(5), Validators.maxLength(50)]],
      name: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      mobileNumber: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      dateOfBirth: [],
      gender: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      password: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      collegeName: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      degree: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      location: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      image: ['',],
      customerId: ['',],
      // animalControl: [{name: 'Dog', sound: 'Woof!'},
      // {name: 'Cat', sound: 'Meow!'},
      // {name: 'Cow', sound: 'Moo!'},
      // {name: 'Fox', sound: 'Wa-pa-pa-pa-pa-pa-pow!'},],
  });
   }
  get f() { return this.addStudentForm.controls; }
  ngOnInit() {
    this.addStudentService.getCustomer().subscribe(
      res => {
    console.log(res)
    this.customer = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }
  preview(files: string | any[]) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }
  back(){
    this.router.navigate(['/trainee/students']);
  }
  
  onSubmit(){
    
    const addStudentData = this.addStudentForm.value;
    console.log(addStudentData)
    const data={
      name:addStudentData.name,
      email:addStudentData.email,
      mobileNumber:addStudentData.mobileNumber,
      dateOfBirth:this.dateOfBirth,
      gender:addStudentData.gender,
      password:addStudentData.password,
      collegeName:addStudentData.collegeName,
      degree:addStudentData.degree,
      location:addStudentData.location,
      customerId:addStudentData.customerId,
    }

    console.log(data)
    this.addStudentService.addStudent(data).subscribe(
      res => {
    console.log(res)
    // this.trainees = res.body['data']
    this.router.navigate(['/trainee/students']);
      },
      err => {
          console.log(err);
      }
  );
  }

  onDateChange(newDate: Date) {
    console.log(newDate);
    function convert(str) {
      var date = new Date(str),
        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
        day = ("0" + date.getDate()).slice(-2);
      return [day, mnth, date.getFullYear()].join("/");
    }
    this.dateOfBirth = convert(newDate);
    console.log(this.dateOfBirth)
  } 
}