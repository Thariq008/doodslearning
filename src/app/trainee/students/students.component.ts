import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../students/students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  students: any;
  customer: any;
  firstCustomerId: any;
  constructor(
    public router: Router,
    private studentService: StudentService
    ) { }

  
  ngOnInit() {
    this.studentService.getCustomer().subscribe(
      res => {
    console.log(res)
    this.customer = res.body['data']
    console.log(this.customer);
    console.log(this.customer[0]._id);
    this.firstCustomerId = this.customer[0]._id;
    this.studentService.getStudent(this.firstCustomerId).subscribe(
      res => {
    console.log(res)
    this.students = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
      },
      err => {
          console.log(err);
      }
  );

    
  }

  selectCollege(customerId){
    console.log(customerId);
    this.studentService.getStudent(customerId).subscribe(
      res => {
    console.log(res)
    this.students = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }

  // customers = [
  //   {colg_name: "hindusthan institute of technology", location: "coimbatore", colg_icon: "../../../assets/img/hind_logo.png", status:"unassigned"},
  //   {colg_name: "hindusthan institute of technology", location: "coimbatore", colg_icon: "../../../assets/img/hind_logo.png", status:"assigned"},
  // ];

  detailview(id){

    this.router.navigate(['/trainee/students/assign/'+id]);
  }
  assign(){
    this.router.navigate(['/trainee/students/assign']);
  }
}
