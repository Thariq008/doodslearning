import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsComponent } from './students.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { AssignComponent } from './assign/assign.component';

const appRoutes: Routes = [
  { path: '', component: StudentsComponent },
  { path: 'add-student', component: AddStudentComponent },
  { path: 'assign', component: AssignComponent },
  { path: 'assign/:id', component: AssignComponent }
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class StudentsRoutingModule { }
