import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { StudentsRoutingModule } from './students-routing.module';
import { StudentsComponent } from './students.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { AssignComponent } from './assign/assign.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [
    StudentsComponent,
    AddStudentComponent,
    AssignComponent
  ],
  imports: [
    SharedModule,
    StudentsRoutingModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot()
  ]
})
export class StudentsModule { }
