import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CompletedCourseService } from './completed-course.service';

@Component({
  selector: 'app-completed-course',
  templateUrl: './completed-course.component.html',
  styleUrls: ['./completed-course.component.scss']
})
export class CompletedCourseComponent implements OnInit {
  courses: any;

  constructor(
    public router: Router,
    public completedCourseService: CompletedCourseService
    ) { }

  ngOnInit() {
    this.CoursesList();
  }
  CoursesList() {
    this.completedCourseService.addTopics().subscribe(
      res => {
    this.courses = res.body['data']
      },
      err => {
          console.log(err);
      }
  );
  }

  detailview(id){
    console.log(id)
    this.router.navigate(['/trainee/daily_class/'+id]);
  }
  coursesView(id){
    this.router.navigate(['/trainee/daily_class/'+id]);
  }
}

