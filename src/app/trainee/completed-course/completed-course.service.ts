import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class CompletedCourseService {

  baseUrl = environment.baseUrl;
  accessToken = sessionStorage.getItem('accessToken');
  constructor(private http: HttpClient) { }
  
  addTopics(){
    const httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.accessToken
      });
      return this.http.get(this.baseUrl + '/course', {
  
        headers: httpHeaders,
        observe: 'response'
      });
  }
}
