import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TraineeComponent } from './trainee.component';
import { TraineeRoutingModule } from './trainee-routing.module';
// import { CoursesListComponent } from './courses-list/courses-list.component'
// import { CoursesViewComponent } from './courses-view/courses-view.component';
// import { StudentListComponent } from './student-list/student-list.component';
// import { AddStudentComponent } from './add-student/add-student.component';
// import { ViewStudentComponent } from './view-student/view-student.component';
import { DashboardTraineeComponent } from './dashboard-trainee/dashboard-trainee.component';
import { CompletedCourseComponent } from './completed-course/completed-course.component';
import { DailyClassComponent } from './daily-class/daily-class.component';
@NgModule({
  declarations: [
    TraineeComponent,
    DashboardTraineeComponent,
    CompletedCourseComponent,
    DailyClassComponent,
  ],
  imports: [
    SharedModule,
    TraineeRoutingModule
    // CoursesListComponent,
    // CoursesViewComponent,
    // CoursesViewComponent,
    // StudentListComponent,
    // AddStudentComponent,
    // ViewStudentComponent,
    // DashboardTraineeComponent
  ]
})
export class TraineeModule { }
