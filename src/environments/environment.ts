// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// import Swal from 'sweetalert2';

export const environment = {
  production: false,
  // baseUrl: 'http://3.17.188.248:3001/',
  // baseUrl: 'http://localhost:3000/',
  baseUrl: 'https://doods-learning.herokuapp.com'
//   swal(type,msg) {
//     if(type==='success') {
//     Swal.fire({
//       title: 'Invalid Login',
//       text: msg,
//       type: 'success',
//       confirmButtonText: 'Ok'
//     });
//     } else {
//       Swal.fire({
//         title: 'Invalid Login',
//         text: msg,
//         type: type,
//         confirmButtonText: 'Ok',
//       });
//     }
//     }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
